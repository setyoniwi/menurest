import 'package:flutter/material.dart';

void main() {
  runApp(RestaurantApp());
}

class RestaurantApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: PilihMenuScreen(),
    );
  }
}

class MenuItem {
  final String name;
  final String description;
  final double price;
  final String imagePath;

  MenuItem({required this.name, required this.description, required this.price, required this.imagePath});
}

class OrderItem {
  final MenuItem menuItem;
  int quantity;

  OrderItem({required this.menuItem, this.quantity = 1});
}

final List<MenuItem> menuItems = [
  MenuItem(
    name: 'Steak',
    description: 'Delicious steak with juisy meat',
    price: 150.0,
    imagePath: 'assets/st.jpeg',
  ),
  MenuItem(
    name: 'Pizza',
    description: 'Delicious pizza with your choice of toppings',
    price: 100.0,
    imagePath: 'assets/pisa.jpeg',
  ),
  MenuItem(
    name: 'Dessert',
    description: 'Delicious dessert with your choice',
    price: 75.0,
    imagePath: 'assets/ds.jpeg',
  ),
  MenuItem(
    name: 'Zuppa Soup',
    description: 'Delicious soup with vegetable toppings',
    price: 50.0,
    imagePath: 'assets/supa.jpeg',
  ),
  MenuItem(
    name: 'Dimsum',
    description: 'Delicious dimsum with chiken',
    price: 30.0,
    imagePath: 'assets/dimsum.jpeg',
  ),
  
];

class PilihMenuScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pilih Menu'),
      ),
      body: ListView.builder(
        itemCount: menuItems.length,
        itemBuilder: (context, index) {
          final menuItem = menuItems[index];
          return ListTile(
            leading: Image.asset(menuItem.imagePath),
            title: Text(menuItem.name),
            subtitle: Text(menuItem.description),
            trailing: Text('\$${menuItem.price.toStringAsFixed(2)}'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PreviewMenuScreen(menuItem: menuItem),
                ),
              );
            },
          );
        },
      ),
    );
  }
}

class PreviewMenuScreen extends StatefulWidget {
  final MenuItem menuItem;

  PreviewMenuScreen({required this.menuItem});

  @override
  _PreviewMenuScreenState createState() => _PreviewMenuScreenState();
}

class _PreviewMenuScreenState extends State<PreviewMenuScreen> {
  int quantity = 1;

  void increaseQuantity() {
    setState(() {
      quantity++;
    });
  }

  void decreaseQuantity() {
    if (quantity > 1) {
      setState(() {
        quantity--;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.menuItem.name),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(widget.menuItem.imagePath),
          Text(widget.menuItem.name),
          Text(widget.menuItem.description),
          Text('\$${(widget.menuItem.price * quantity).toStringAsFixed(2)}'),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: Icon(Icons.remove),
                onPressed: decreaseQuantity,
              ),
              Text(quantity.toString()),
              IconButton(
                icon: Icon(Icons.add),
                onPressed: increaseQuantity,
              ),
            ],
          ),
          ElevatedButton(
            onPressed: () {
              final orderedItem = OrderItem(menuItem: widget.menuItem, quantity: quantity);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HasilPemesananScreen(orderedItems: [orderedItem]),
                ),
              );
            },
            child: Text('Pesan Menu'),
          ),
        ],
      ),
    );
  }
}

class HasilPemesananScreen extends StatelessWidget {
  final List<OrderItem> orderedItems;

  HasilPemesananScreen({required this.orderedItems});

  @override
  Widget build(BuildContext context) {
    double total = 0.0;
    orderedItems.forEach((item) {
      total += item.menuItem.price * item.quantity;
    });

    return Scaffold(
      appBar: AppBar(
        title: Text('Hasil Pemesanan'),
      ),
      body: ListView.builder(
        itemCount: orderedItems.length,
        itemBuilder: (context, index) {
          final item = orderedItems[index];
          return ListTile(
            title: Text(item.menuItem.name),
            subtitle: Text('Jumlah: ${item.quantity} x \$${item.menuItem.price.toStringAsFixed(2)}'),
            trailing: Text('\$${(item.menuItem.price * item.quantity).toStringAsFixed(2)}'),
          );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Total: \$${total.toStringAsFixed(2)}'),
              ElevatedButton(
  onPressed: () {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PilihMenuScreen(),
      ),
    );
  },
  child: Text('Kembali ke Pilih Menu'),
),
            ],
          ),
        ),
      ),
    );
  }
}
